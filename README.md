# scientific_papers_scraper

 Scraper to download the newest scientific articles in PDF format,
 available for free and gathered in google scholar,
 about given phrase as argument to script

## To proper work of the program you'll need:
*  python 2.7
*  mechanize module to python 2
*  Beautiful Soup 4 module for python 2
*  lxml - html and xml parser for python 2


 The best practise is to create virtualenv and install there
 all required dependancies becouse packages installed on your
 computer can cause some problems and crashes

 NOTE! that Google has limit of daily queries and after this limit
 it requires to do captcha quiz to prove that you are not a robot
 and this script is not captchaproof and cannot resolve this

## Usage:
`       $ python scientific_scraper.py "Your phrase to search"`
       
 NOTE! that you have to pass your phrase in quotes

