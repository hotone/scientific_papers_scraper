#-*- coding: utf-8 -*-
import sys
import mechanize
import re
import urllib2

from urllib2 import quote
from bs4 import BeautifulSoup

def openPageAndGetHTML(URL):
    try:
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1)Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
        response = br.open(URL)
        html = response.read()

        return html
    except:
        print("Cannot open first site, check your connection")
        sys.exit()

def scrapeLinksToPDFs(html):

    soup = BeautifulSoup(html, 'lxml')
    data = soup.findAll('div', attrs={'class':'gs_or_ggsm'})

    listOflinks = []

    for div in data:
        links = div.findAll('a')
        for a in links:
            #Add just PDF to the list, and skip HTML
            if(re.search("[PDF]", a.text)):
                print a.text
                listOflinks.append(a['href'])
    return listOflinks

def scrapeLinksToNextPages(html):
    soup = BeautifulSoup(html, 'lxml')
    nextPage = soup.findAll('div', attrs={'id':'gs_n'})

    nextPagesToScrape = []

    for navigation in nextPage:
        lin = navigation.findAll('a')
        for a in lin:
            nextPagesToScrape.append("https://scholar.google.pl"+a['href']+"/")

    return nextPagesToScrape
#from list of lists make one list with all links
def prepareFinalLinkList(nestedList):
    finalList = []
    for lists in nestedList:
        for i in lists:
            finalList.append(i)
    return finalList

def downloadListOfPDFs(downloadList):
    amountOfPDFinList = len(downloadList)
    for index, pdf in enumerate(downloadList, start=1):
        try:
            #as a name of file take everything after last slash "/" in url
            nameOfFile = pdf.rsplit('/',1)[-1]
            downloadedFile = urllib2.urlopen(pdf)
            currentDownloadedFile = index
            with open(nameOfFile, 'wb') as output:
                print("Downloading ({0}/{1}): {2}".format(currentDownloadedFile, amountOfPDFinList, pdf))
                output.write(downloadedFile.read())
        except:
            print("WARNING!!! Download of {} failed!".format(pdf))

if __name__ == "__main__":

    #first check if arguments are passed properly, else close program
    #and show how to do it right way
    if(len(sys.argv) < 2):
        print('\nToo few arguments! You have to give phrase to search like in example below:\n')
        print('\t------------------------------------------------------------------------------\n')
        print('\t$ python scipaper_scraper.py "here (in quotes) give your phrase to search"\n')
        print('\t------------------------------------------------------------------------------\n')
        sys.exit()

    elif(len(sys.argv) > 2):
        print('\nToo many arguments! You have to give phrase to search like in example below:\n')
        print('\t------------------------------------------------------------------------------\n')
        print('\t$ python scipaper_scraper.py "here (in quotes) give your phrase to search"\n')
        print('\t------------------------------------------------------------------------------\n')
        sys.exit()

    #if there is no problem with argument do the main program
    else:
        #take phrase from argument and create url with this
        phraseToSearch = sys.argv[1]

        preparedURL = phraseToSearch.lower().replace(" ", "+")

        escapedURL = quote(preparedURL.encode('utf-8'))

        #url is created with option that sort results by date - the newest ones, in all languages
        #and phrase is pasted to "q=" parameter in URL where all letters
        #are changed to lowercase and all spaces are replaced with "+"
        readyURL="https://scholar.google.pl/scholar?hl=pl&scisbd=1&as_sdt=1%2C5&as_vis=1&q="+escapedURL+"&btnG="

        print("-----------------------------------------------------------")
        print("Your link created with passed phrase:")
        print(readyURL)
        print("-----------------------------------------------------------")

        listOfPDFs = []

        #open first page with newest articles by URL created from phrase, which has been passed as
        #argument during running of the script
        html = openPageAndGetHTML(readyURL)

        #scrape list of links to the following 10 sites

        print("Downloading the links to next sites")
        print("Opening and scraping links to PDFs from the next pages")
        #print scrapeLinksToNextPages(html)
        linksToScrape = scrapeLinksToNextPages(html)

        #get HTML gained from opened site and parse to find all available links to PDFs
        print("-----------------------------------------------------------")

        listOfPDFs.append(scrapeLinksToPDFs(html))

        for link in linksToScrape:
            htmlFromLink = openPageAndGetHTML(link)
            links = scrapeLinksToPDFs(htmlFromLink)
            listOfPDFs.append(links)

        #print listOfPDFs

        print("-----------------------------------------------------------")
        #print(prepareFinalLinkList(listOfPDFs))
        print("Making list of links to download")
        print("-----------------------------------------------------------")

        listToDownload = prepareFinalLinkList(listOfPDFs)

        print("-----------------------------------------------------------")
        print("Downloading prepared list of PDF's")
        print("-----------------------------------------------------------")

        downloadListOfPDFs(listToDownload)

        print("-----------------------------------------------------------")
        print("Downloading completed!!! Enjoy :)")
        print("-----------------------------------------------------------")
